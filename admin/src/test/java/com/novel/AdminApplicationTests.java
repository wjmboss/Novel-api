package com.novel;

import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AdminApplicationTests {
    // 注入StringEncryptor
    @Autowired
    private StringEncryptor stringEncryptor;

    @Test
    public void contextLoads() {
        // 加密数据库用户名
        String username = stringEncryptor.encrypt("root");
        System.out.println(username);
    }

}
