package com.novel.system.mapper;


import com.novel.system.domain.SysOperLog;

import java.util.List;

/**
 * 操作日志 数据层
 *
 * @author novel
 * @date 2019/5/14
 */
public interface SysOperLogMapper {
    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     * @return 结果
     */
    int insertOperlog(SysOperLog operLog);

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    List<SysOperLog> selectOperLogList(SysOperLog operLog);

    /**
     * 批量删除系统操作日志
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    int deleteOperLogByIds(Integer[] ids);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    SysOperLog selectOperLogById(Integer operId);

    /**
     * 清空操作日志
     *
     * @return 结果
     */
    int cleanOperLog();
}
