package com.novel.system.controller;

import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.shiro.utils.ShiroUtils;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysRole;
import com.novel.system.service.SysMenuService;
import com.novel.system.service.SysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色信息
 *
 * @author novel
 * @date 2019/4/30
 */
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController {
    private final SysRoleService roleService;
    private final SysMenuService sysMenuService;

    public SysRoleController(SysRoleService roleService, SysMenuService sysMenuService) {
        this.roleService = roleService;
        this.sysMenuService = sysMenuService;
    }

    /**
     * 获取角色列表
     *
     * @param role 查询条件
     * @return 角色列表
     */
    @RequiresPermissions("system:role:list")
    @GetMapping("/list")
    public TableDataInfo list(SysRole role) {
        startPage();
        List<SysRole> list = roleService.selectRoleList(role);
        return getDataTable(list);
    }

    /**
     * 保存编辑角色信息
     *
     * @param role 角色信息
     * @return 操作结果
     */
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:role:add")
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysRole role) {
        validate(role);
        role.setCreateBy(ShiroUtils.getUserName());
        return toAjax(roleService.insertRole(role), "角色新增成功", "角色新增失败");
    }

    /**
     * 保存编辑角色信息
     *
     * @param role 角色信息
     * @return 操作结果
     */
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:role:edit")
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysRole role) {
        validate(role);
        boolean result = roleService.updateRole(role);
        if (result) {
            ShiroUtils.clearCachedAuthorizationInfo();
        }
        return toAjax(result, "角色修改成功", "角色修改失败");
    }

    /**
     * 唯一验证
     *
     * @param role
     */
    private void validate(SysRole role) {
        if (UserConstants.ROLE_NAME_NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            throw new BusinessException("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
        } else if (UserConstants.ROLE_KEY_NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            throw new BusinessException("新增角色'" + role.getRoleName() + "'失败，权限字符已存在");
        }

        role.setUpdateBy(ShiroUtils.getUserName());
    }

    /**
     * 删除角色信息
     *
     * @param ids 角色ID
     * @return 操作结果
     */
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:role:remove")
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        boolean result = roleService.deleteRoleByIds(ids);
        if (result) {
            ShiroUtils.clearCachedAuthorizationInfo();
        }
        return toAjax(result, "角色删除成功", "角色删除失败");
    }

    /**
     * 加载角色菜单列表树
     */
    @RequiresPermissions("system:role:edit")
    @GetMapping("/roleMenuTreeData")
    public Result roleMenuTreeData(SysRole role) {
        Map<String, Object> map = sysMenuService.roleMenuTreeData(role);
        return toAjax(map);
    }

    /**
     * 校验角色名称
     *
     * @return 结果
     */
    @PostMapping("/checkRoleNameUnique")
    public Result checkRoleNameUnique(SysRole role) {
        return toAjax(roleService.checkRoleNameUnique(role));
    }

    /**
     * 校验角色权限
     *
     * @return 结果
     */
    @PostMapping("/checkRoleKeyUnique")
    public Result checkRoleKeyUnique(SysRole role) {
        return toAjax(roleService.checkRoleKeyUnique(role));
    }


    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:role:export")
    @GetMapping("/export")
    public Result export(SysRole role) {
        startPage();
        List<SysRole> roleList = roleService.selectRoleList(role);
        String fileName = ExcelUtils.exportExcelToFile(roleList, "角色管理", SysRole.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}