package com.novel.system.controller;

import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysOperLog;
import com.novel.system.service.SysOperLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 操作日志记录
 *
 * @author novel
 * @date 2019/12/20
 */
@RestController
@RequestMapping("/system/logs")
public class SysOperlogController extends BaseController {

    private final SysOperLogService operLogService;

    public SysOperlogController(SysOperLogService operLogService) {
        this.operLogService = operLogService;
    }

    @RequiresPermissions("system:logs:list")
    @GetMapping("/list")
    public TableDataInfo list(SysOperLog operLog) {
        startPage();
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        return getDataTable(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:logs:remove")
    @DeleteMapping("/remove")
    public Result remove(Integer[] ids) {
        return toAjax(operLogService.deleteOperLogByIds(ids));
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @RequiresPermissions("system:logs:clean")
    @DeleteMapping("/clean")
    public Result clean() {
        operLogService.cleanOperLog();
        return success();
    }


    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:logs:export")
    @GetMapping("/export")
    public Result export(SysOperLog operLog) {
        startPage();
        List<SysOperLog> logList = operLogService.selectOperLogList(operLog);
        String fileName = ExcelUtils.exportExcelToFile(logList, "操作日志", SysOperLog.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}
