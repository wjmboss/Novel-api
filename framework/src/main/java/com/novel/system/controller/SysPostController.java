package com.novel.system.controller;

import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.shiro.utils.ShiroUtils;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysPost;
import com.novel.system.service.SysPostService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 岗位信息操作处理
 *
 * @author novel
 * @date 2019/6/11
 */
@RestController
@RequestMapping("/system/post")
public class SysPostController extends BaseController {
    private final SysPostService postService;

    public SysPostController(SysPostService postService) {
        this.postService = postService;
    }


    @RequiresPermissions("system:post:list")
    @GetMapping("/list")
    public TableDataInfo list(SysPost post) {
        startPage();
        List<SysPost> list = postService.selectPostList(post);
        return getDataTable(list);
    }


    @RequiresPermissions("system:post:remove")
    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/remove")
    public Result remove(String ids) {
        return toAjax(postService.deletePostByIds(ids));
    }

    /**
     * 新增保存岗位
     */
    @RequiresPermissions("system:post:add")
    @Log(title = "岗位管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysPost post) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setCreateBy(ShiroUtils.getUserName());
        return toAjax(postService.insertPost(post));
    }


    /**
     * 修改保存岗位
     */
    @RequiresPermissions("system:post:edit")
    @Log(title = "岗位管理", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysPost post) {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (UserConstants.POST_CODE_NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            throw new BusinessException("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setUpdateBy(ShiroUtils.getUserName());
        return toAjax(postService.updatePost(post));
    }

    /**
     * 校验岗位名称
     *
     * @return 结果
     */
    @PostMapping("/checkPostNameUnique")
    public Result checkPostNameUnique(SysPost post) {
        return toAjax(postService.checkPostNameUnique(post));
    }

    /**
     * 校验岗位编码
     *
     * @return 结果
     */
    @PostMapping("/checkPostCodeUnique")
    public Result checkPostCodeUnique(SysPost post) {
        return toAjax(postService.checkPostCodeUnique(post));
    }


    @Log(title = "岗位管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:post:export")
    @GetMapping("/export")
    public Result export(SysPost post) {
        startPage();
        List<SysPost> postList = postService.selectPostList(post);
        String fileName = ExcelUtils.exportExcelToFile(postList, "岗位管理", SysPost.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}
