package com.novel.system.controller;

import com.novel.framework.base.BaseController;
import com.novel.framework.config.ProjectConfig;
import com.novel.framework.result.Result;
import com.novel.kaptcha.adapter.Kaptcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 验证码控制器
 *
 * @author novel
 * @date 2019/12/2
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController extends BaseController {

    @Resource
    private Kaptcha kaptcha;
    private final ProjectConfig projectConfig;

    public CommonController(ProjectConfig projectConfig) {
        this.projectConfig = projectConfig;
    }


    /**
     * 验证码生成
     *
     * @return 验证码
     */
    @GetMapping(value = "/captchaImage")
    public Result getKaptchaImage() {
        return toAjax(kaptcha.writeBase64Code());
    }

    /**
     * 获取项目基本信息
     *
     * @return 项目基本信息
     */
    @GetMapping(value = "/getProjectInfo")
    public Result getProjectInfo() {
        return toAjax(projectConfig.getProjectInfo());
    }
}
