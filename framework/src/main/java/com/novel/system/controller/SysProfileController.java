package com.novel.system.controller;

import cn.hutool.core.bean.BeanUtil;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.system.domain.SysUser;
import com.novel.system.service.SysMenuService;
import com.novel.system.service.SysUserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 个人信息 业务处理
 *
 * @author novel
 * @date 2019/4/29
 */
@RestController
@RequestMapping("/system/user")
public class SysProfileController extends BaseController {
    private final SysMenuService sysMenuService;
    private final SysUserService sysUserService;

    public SysProfileController(SysMenuService sysMenuService, SysUserService sysUserService) {
        this.sysMenuService = sysMenuService;
        this.sysUserService = sysUserService;
    }

    /**
     * 获取当前用户登录信息，用户基本信息、权限、菜单
     */
    @GetMapping("/getLoginUserInfo")
    public Result getLoginUserInfo() {
        Map<String, Object> map = new HashMap<>(3);
        SysUser user = getUser();
        SysUser sysUser = new SysUser();
        BeanUtil.copyProperties(user, sysUser);
        Set<String> perms = sysMenuService.selectPermsByUser(user);
        map.put("menu", sysMenuService.selectMenusByUser(user));
        map.put("permission", perms);
        map.put("user", sysUser);
        return Result.success(map);
    }

    /**
     * 获取当前登录用户详细信息，用户基本信息、部门
     */
    @GetMapping("/getUserInfo")
    public Result getUserInfo() {
        SysUser sysUser = sysUserService.selectUserByIdAndDept(getUserId());
        return Result.success(sysUser);
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public Result update(@Validated(EditGroup.class) SysUser user) {
        if (sysUserService.updateUserInfo(user)) {
            SysUser sysUser = sysUserService.selectUserById(user.getId());

            SysUser userCopy = new SysUser();
            BeanUtil.copyProperties(sysUser, userCopy);
            setUser(sysUser);
            return toAjax(userCopy);
        }
        return error();
    }

    /**
     * 修改密码
     *
     * @param oldPassWord 旧密码
     * @param newPassWord 新密码
     * @return 结果
     */
    @Log(title = "修改密码", businessType = BusinessType.UPDATE)
    @PutMapping("/modifyPassword")
    @Validated
    public Result resetPwd(@NotBlank(message = "旧密码不能为空") String oldPassWord,
                           @NotBlank(message = "新密码不能为空") @Size(max = 16, message = "新密码长度不能超过16个字符") String newPassWord) {
        SysUser user = getUser();
        if (sysUserService.modifyPassword(user, oldPassWord, newPassWord)) {
            setUser(sysUserService.selectUserById(user.getId()));
            return success("密码设置成功");
        }
        return error("密码设置成功");
    }
}