package com.novel.system.controller;

import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysLogininfor;
import com.novel.system.service.SysLogininforService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统访问记录
 *
 * @author novel
 * @date 2019/12/20
 */
@RestController
@RequestMapping("/system/logininfor")
public class SysLogininforController extends BaseController {

    private final SysLogininforService logininforService;

    public SysLogininforController(SysLogininforService logininforService) {
        this.logininforService = logininforService;
    }

    /**
     * 查看日志列表
     *
     * @param logininfor 查询条件
     * @return 结果
     */
    @RequiresPermissions("system:logininfor:list")
    @GetMapping("/list")
    public TableDataInfo list(SysLogininfor logininfor) {
        startPage();
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        return getDataTable(list);
    }

    /**
     * 删除登录日志
     *
     * @param ids 日志id
     * @return 结果
     */
    @RequiresPermissions("system:logininfor:remove")
    @Log(title = "登陆日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        return toAjax(logininforService.deleteLogininforByIds(ids));
    }

    /**
     * 清空登录日志
     *
     * @return 结果
     */
    @RequiresPermissions("system:logininfor:clean")
    @Log(title = "登陆日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public Result clean() {
        logininforService.cleanLogininfor();
        return success();
    }

    /**
     * 导出操作日志
     *
     * @param logininfor 查询条件
     * @return 结果
     */
    @Log(title = "登陆日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:logininfor:export")
    @GetMapping("/export")
    public Result export(SysLogininfor logininfor) {
        startPage();
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        String fileName = ExcelUtils.exportExcelToFile(list, "登录日志", SysLogininfor.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}
