package com.novel.system.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * 用户导入模板
 *
 * @author novel
 * @date 2020/3/3
 */
@Data
public class SysUserVo {
    @Excel(name = "登录名称")
    private String userName;
    @Excel(name = "部门编号")
    private Long deptId;
    @Excel(name = "用户姓名")
    private String name;
    @Excel(name = "邮箱", width = 15)
    private String email;
    @Excel(name = "手机号码", width = 15)
    private String phoneNumber;
    @Excel(name = "用户性别", replace = {"男_0", "女_1", "未知_2"})
    private String sex;
    @Excel(name = "帐号状态", replace = {"正常_0", "停用_1"})
    private String status;
}
