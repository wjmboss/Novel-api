package com.novel.framework.redis.cache;

import cn.hutool.core.util.ObjectUtil;
import com.novel.common.constants.Constants;
import com.novel.framework.redis.ICacheService;
import com.novel.framework.shiro.config.JwtProperties;
import com.novel.framework.shiro.jwt.utils.JWTUtil;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * shiro redis 缓存操作类
 *
 * @author novel
 * @date 2019/4/16
 */
public class ShiroRedisCache<K, V> implements Cache<K, V> {
    private String prefix;
    private JwtProperties jwtProperties;

    private ICacheService iCacheService;

    public String getPrefix() {
        return prefix + ":";
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public ShiroRedisCache(ICacheService iCacheService) {
        this.iCacheService = iCacheService;
        this.prefix = Constants.SHIRO_REDIS_PREFIX;
    }

    public ShiroRedisCache(ICacheService iCacheService, JwtProperties jwtProperties) {
        this(iCacheService);
        this.jwtProperties = jwtProperties;
    }

    public ShiroRedisCache(ICacheService iCacheService, String prefix) {
        this(iCacheService);
        this.prefix = this.getPrefix() + prefix;
    }

    public ShiroRedisCache(ICacheService iCacheService, JwtProperties jwtProperties, String prefix) {
        this(iCacheService, jwtProperties);
        this.prefix = this.getPrefix() + prefix;
    }

    @Override
    public V get(K k) throws CacheException {
        if (k == null) {
            return null;
        }
        return (V) iCacheService.get(getKey(k));

    }

    @Override
    public V put(K k, V v) throws CacheException {
        if (k == null || v == null) {
            return null;
        }
        iCacheService.set(getKey(k), v, jwtProperties.getShiroCacheExpireTime(), TimeUnit.MINUTES);
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        if (k == null) {
            return null;
        }
        V v = (V) iCacheService.get(getKey(k));
        iCacheService.remove(getKey(k));
        return v;
    }

    @Override
    public void clear() throws CacheException {
        iCacheService.removePattern(getPrefix() + "*");
    }

    @Override
    public int size() {
        return keys().size();
    }

    @Override
    public Set<K> keys() {
        Set<String> keys = iCacheService.keys(getPrefix() + "*");

        Set<K> sets = new HashSet<>();
        assert keys != null;
        for (String key : keys) {
            sets.add((K) key);
        }
        return sets;
    }

    @Override
    public Collection<V> values() {
        Set<K> keys = keys();
        List<V> values = new ArrayList<>(keys.size());
        for (K k : keys) {
            values.add(get(k));
        }
        return values;
    }

    private String getKey(K key) {
        if (key instanceof String) {
            return this.getPrefix() + JWTUtil.getSubject(key.toString()) + ":" + JWTUtil.getId(key.toString());
        } else {
            return ObjectUtil.toString(this.getPrefix() + JWTUtil.getSubject(key.toString()) + ":" + JWTUtil.getId(key.toString()));
        }
    }

}