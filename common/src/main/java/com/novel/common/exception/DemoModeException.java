package com.novel.common.exception;

/**
 * 演示模式异常
 *
 * @author novel
 * @date 2019/12/5
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
    }
}
