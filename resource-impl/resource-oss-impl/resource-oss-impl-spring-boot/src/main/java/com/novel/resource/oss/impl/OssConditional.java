package com.novel.resource.oss.impl;


import com.novel.common.resource.ResourceType;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * oss 注入控制
 *
 * @author novel
 * @date 2019/6/10
 */
public class OssConditional implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        ResourceType type = context.getEnvironment().getProperty("resource.file-type", ResourceType.class);
        return ResourceType.OSS.equals(type);
    }
}
