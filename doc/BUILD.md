#### [介绍文档](../README.md) | 部署文档 | [初始化文档](./INIT.md)

### 准备工作
```
JDK >= 1.8 (推荐1.8版本)
MySQL >= 5.7 (推荐5.7版本)
Maven >= 3.0
Redis >= 3.2 (推荐3.2版本)
```
其他`（非必须）`
```
阿里云对象存储OSS、腾讯云对象存储COS以及MinIo存储
```

### 运行系统

* 将下载好的项目导入到idea中。
* 在数据库中创建`novel`数据库并执行sql文件夹下的`novel.sql`脚本。
* 运行`admin`模块下的`NovelApplication.java`。
* 直至控制台中打出启动系统，并且没有任何异常，则启动成功。

### 必要配置
1. 项目的文件存储根目录，如果是`windows`系统，请根据自己的盘符进行相应的配置。
2. 修改数据库信息。
3. 修改`redis`信息。
4. 修改资源存储配置信息`resource`。

#### mysql:

编辑`admin`模块`resources`目录下的`application-dev.yml`
```yaml
spring:
  datasource:
    druid:
      #数据库连接url
      url: 
      #数据库连接用户名
      username: 
      #数据库连接密码
      password: 
```

#### redis:
编辑`admin`模块`resources`目录下的`application-dev.yml`
```yaml
spring:
  redis:
    #redis地址
    host: 
    #redis密码
    password: 
    #redis端口
    port: 
```

#### `resource`文件存储配置:

编辑`admin`模块`resources`目录下的`application-dev.yml`

```yaml
resource:
  #是否开启文件服务器存储文件（默认关闭）
  enable: 
  #文件服务器类型（暂时支持oss和cos）
  file-type: 
  # 是否对文件服务器的文件进行本地缓存（默认关闭）
  cache-enable: 
```


### 部署系统
1. 配置好上面的配置。
2. 在项目根目录下运行`mvn clean package -Dmaven.test.skip=true`命令，等待编译完成。
3. 把`docker`文件夹下的`jar`包放到服务器上运行即可。


### 通过docker容器部署
1. 按照上述文档将程序编译完成。
2. 将`docker`文件夹下`builddocker.sh`、`Dockerfile`和编译好的`jar`文件夹拷贝到`docker`服务器上。
3. 执行

```bash
$ sudo chmod 777 builddocker.sh

$ ./builddocker.sh
```

### 配置项加密说明
1. 在配置文件中加入`jasypt.encryptor.password`配置信息。（`这个密码是用来对配置进行加解密的秘钥，一定妥善保管`）
2. 运行`admin`模块下的`AdminApplicationTests.java`中的测试方法，把`root`字符串替换为自己想要加密的属性配置项。
3. 将得到的加密后的配置项配置到对应的配置上，且用`ENC()`包裹，例如：`ENC(XW2daxuaTftQ+F2iYPQu0g==)`，括号中间的则是配置信息。
4. 配置信息配置完后删除`jasypt.encryptor.password`配置项。
5. 在启动的环境中加入`jasypt.encryptor.password`环境信息，例如在`Dockerfile`文件中启动程序时加入了`"-Djasypt.Encryptor.Password=VLu3H58dxYAsv3TIGOueaXIXBbhbT2"`信息，这里的密码和加密时使用的一致。

### 文件服务器配置说明
如果不使用文件服务器，则把`resource.enable`配置为`false`或者不配置，其他项均不用配置，默认存储在程序所在的服务器上。

#### 使用阿里云OSS对象存储：

1. 在`pom.xml`中加入
```xml
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>oss-spring-boot-starter</artifactId>
</dependency>

<dependency>
    <groupId>com.novel</groupId>
    <artifactId>resource-spring-boot-starter</artifactId>
</dependency>
```
2. 配置OSS信息：
```yaml
resource:
  #是否开启文件服务器存储文件（默认关闭）
  enable: true
  #文件服务器类型（暂时支持oss和cos）
  file-type: oss
oss:
  #容器访问id
  oss-access-key-id: 
  #容器访问秘钥
  oss-access-key-secret: 
  #oss容器地址
  oss-endpoint: 
  #容器的名称（如果不存在，则会在程序启动时自动创建，中途如果用户删除了，则不会创建，需要重新启动程序或者手动创建）
  bucket-name: 
```

#### 使用腾讯云COS对象存储：

1. 在`pom.xml`中加入
```xml
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>cos-spring-boot-starter</artifactId>
</dependency>

<dependency>
    <groupId>com.novel</groupId>
    <artifactId>resource-spring-boot-starter</artifactId>
</dependency>
```
2. 配置OSS信息：
```yaml
resource:
  #是否开启文件服务器存储文件（默认关闭）
  enable: true
  #文件服务器类型（暂时支持oss和cos）
  file-type: cos
cos:
  #数据桶的名称（如果不存在，则会在程序启动时自动创建，中途如果用户删除了，则不会创建，需要重新启动程序或者手动创建）
  bucket-name: 
  #区域
  region: 
  #访问容器的id
  secret-id: 
  #访问容器的密钥
  secret-key: 
```
#### 使用MinIo存储

1. 在`pom.xml`中加入
```xml
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>minio-spring-boot-starter</artifactId>
</dependency>

<dependency>
    <groupId>com.novel</groupId>
    <artifactId>resource-spring-boot-starter</artifactId>
</dependency>
```
2. 配置MinIo信息：
```yaml
resource:
  #是否开启文件服务器存储文件（默认关闭）
  enable: true
  #文件服务器类型（暂时支持oss和cos）
  file-type: minio
minio:
  # 连接url地址
  url: http://10.168.1.97
  # 服务端口
  port: 9000
  # 访问使用的key
  access-key: minioadmin
  # 访问使用的秘钥
  secret-key: minioadmin
  # 存储文件的桶（如果不存在，则会在程序启动时自动创建，中途如果用户删除了，则不会创建，需要重新启动程序或者手动创建）
  bucket-name: novel
```
官方文档: [MinIo](https://docs.min.io/cn/)

### 通过jenkins部署

暂略


### 常见问题
1. 如果使用`Mac`需要修改`application-dev.yml`文件路径`profile`。
2. 如果使用`Linux`提示表不存在，设置大小写敏感配置在`/etc/my.cnf`添加`lower_case_table_names=1`，重启MYSQL服务。
3. 如果提示当前权限不足，无法写入文件请检查`profile`配置目录是否可读可写，或者无法访问此目录。












