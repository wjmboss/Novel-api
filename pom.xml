<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.novel</groupId>
    <artifactId>novel</artifactId>
    <version>1.3.0</version>
    <packaging>pom</packaging>

    <name>novel</name>
    <url>http://www.cnovel.club</url>
    <description>novel 管理系统</description>

    <modules>
        <module>common</module>
        <module>framework</module>
        <module>admin</module>
        <module>resource-impl</module>
        <module>kaptcha</module>
        <module>quartz</module>
    </modules>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <pagehelper.boot.version>1.2.13</pagehelper.boot.version>
        <druid.boot.version>1.1.21</druid.boot.version>
        <mybatis.boot.version>2.1.1</mybatis.boot.version>
        <shiro-spring.version>1.5.1</shiro-spring.version>
        <swagger.version>2.9.2</swagger.version>
        <hutool.version>5.1.5</hutool.version>
        <jedis.version>3.2.0</jedis.version>
        <fastjson.version>1.2.66</fastjson.version>
        <jwt.version>3.10.0</jwt.version>
        <oshi.version>4.4.2</oshi.version>
        <jackson.version>2.10.2</jackson.version>
        <fileupload.version>1.4</fileupload.version>
        <novel.version>${project.version}</novel.version>
        <springboot.version>2.2.5.RELEASE</springboot.version>
        <jna.version>5.5.0</jna.version>
        <kaptcha.version>2.3.2</kaptcha.version>
        <jasypt.version>3.0.2</jasypt.version>
        <bitwalker.version>1.21</bitwalker.version>
        <easypoi.version>4.1.3</easypoi.version>
        <cos.version>5.6.16</cos.version>
        <minio.version>6.0.13</minio.version>
        <oss.version>3.8.1</oss.version>
    </properties>
    <!-- 依赖声明 -->
    <dependencyManagement>
        <dependencies>
            <!-- SpringBoot的依赖配置-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${springboot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!--druid数据源-->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid.boot.version}</version>
            </dependency>
            <!--mybatis-->
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis.boot.version}</version>
            </dependency>
            <!-- pagehelper 分页插件 -->
            <dependency>
                <groupId>com.github.pagehelper</groupId>
                <artifactId>pagehelper-spring-boot-starter</artifactId>
                <version>${pagehelper.boot.version}</version>
            </dependency>
            <!-- jackson -->
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <!--redis客户端-->
            <dependency>
                <groupId>redis.clients</groupId>
                <artifactId>jedis</artifactId>
                <version>${jedis.version}</version>
            </dependency>
            <!--shiro-->
            <dependency>
                <groupId>org.apache.shiro</groupId>
                <artifactId>shiro-spring</artifactId>
                <version>${shiro-spring.version}</version>
            </dependency>
            <!--jwt-->
            <dependency>
                <groupId>com.auth0</groupId>
                <artifactId>java-jwt</artifactId>
                <version>${jwt.version}</version>
            </dependency>
            <!--开发常用工具-->
            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>
            <!--json-->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>${fastjson.version}</version>
            </dependency>
            <!-- 获取系统信息 -->
            <dependency>
                <groupId>com.github.oshi</groupId>
                <artifactId>oshi-core</artifactId>
                <version>${oshi.version}</version>
            </dependency>

            <dependency>
                <groupId>net.java.dev.jna</groupId>
                <artifactId>jna</artifactId>
                <version>${jna.version}</version>
            </dependency>

            <dependency>
                <groupId>net.java.dev.jna</groupId>
                <artifactId>jna-platform</artifactId>
                <version>${jna.version}</version>
            </dependency>
            <!-- 文件上传工具类 -->
            <dependency>
                <groupId>commons-fileupload</groupId>
                <artifactId>commons-fileupload</artifactId>
                <version>${fileupload.version}</version>
            </dependency>
            <!-- 通用工具-->
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>common</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <!-- 核心模块-->
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>framework</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <!--任务调度模块-->
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>quartz</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>resource-spring-boot-starter</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>oss-spring-boot-starter</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>minio-spring-boot-starter</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>cos-spring-boot-starter</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <!--验证码 -->
            <dependency>
                <groupId>com.github.penggle</groupId>
                <artifactId>kaptcha</artifactId>
                <version>${kaptcha.version}</version>
                <exclusions>
                    <exclusion>
                        <artifactId>javax.servlet-api</artifactId>
                        <groupId>javax.servlet</groupId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>com.github.ulisesbocchio</groupId>
                <artifactId>jasypt-spring-boot-starter</artifactId>
                <version>${jasypt.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>resource-oss-impl-spring-boot</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>resource-cos-impl-spring-boot</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>resource-minio-impl-spring-boot</artifactId>
                <version>${novel.version}</version>
            </dependency>
            <!-- 解析客户端操作系统、浏览器等 -->
            <dependency>
                <groupId>eu.bitwalker</groupId>
                <artifactId>UserAgentUtils</artifactId>
                <version>${bitwalker.version}</version>
            </dependency>

            <dependency>
                <groupId>com.novel</groupId>
                <artifactId>kaptcha</artifactId>
                <version>${novel.version}</version>
            </dependency>

            <dependency>
                <groupId>cn.afterturn</groupId>
                <artifactId>easypoi-spring-boot-starter</artifactId>
                <version>${easypoi.version}</version>
            </dependency>
            <dependency>
                <groupId>com.qcloud</groupId>
                <artifactId>cos_api</artifactId>
                <version>${cos.version}</version>
            </dependency>
            <dependency>
                <groupId>io.minio</groupId>
                <artifactId>minio</artifactId>
                <version>${minio.version}</version>
            </dependency>
            <dependency>
                <groupId>com.aliyun.oss</groupId>
                <artifactId>aliyun-sdk-oss</artifactId>
                <version>${oss.version}</version>
            </dependency>
        </dependencies>

    </dependencyManagement>
    <dependencies>
        <!--lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>public</id>
            <name>aliyun nexus</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

</project>